var ebay = require('./eba.js');
var inserting_req_data = require('./mysq.js').inserting_req_data;
var update = require('./mysq.js').update;
var inserting_resp_data = require('./mysq.js').inserting_resp_data;

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'comp'
});



function finde(req){

var params = {
   keywords: [req.query_string],

   paginationInput: {
      entriesPerPage: 10
   },
  // add additional fields
    outputSelector: ['AspectHistogram'],


    itemFilter: [
     {name: 'FreeShippingOnly', value: true},
      {name: 'MaxPrice', value: '150'}
    ],

  };

  ebay.xmlRequest({
      serviceName: 'Finding',
      opType: 'findItemsByKeywords',
     appId: 'MohamedA-ebayprod-PRD-738c4f481-4f66c90e',      // FILL IN YOUR OWN APP KEY, GET ONE HERE: https://publisher.ebaypartnernetwork.com/PublisherToolsAPI
     params: params,
     parser: ebay.parseResponseJson    // (default)
    },
  // gets all the items together in a merged array
    function itemsCallback(error, itemsResponse) {
      if (error) 
        req.status = 8;

      var items = itemsResponse.searchResult.item;
      connection.query("SELECT id from hh_balrog_request ORDER BY id DESC LIMIT 1", function(err, rows){
        if(err) {
          throw err;
        } else {
            //Defining Object to insert in request database
           var dat = {
            request_id : rows[0].id,
            prod_url :  null,
            img_url : null,
            insert_datetime : null,
            process_id : '',
            status : '9'
           };
           // Updating Country
          update(items[0].globalId.charAt(5)+items[0].globalId.charAt(6), dat);
          //Adding results in response db
          for (var i = 0; i < items.length; i++) {
            dat.prod_url = items[i].viewItemURL;
            dat.img_url = items[i].galleryURL;
            dat.insert_datetime = new Date();
            inserting_resp_data(dat);
          }  
        
        }
      });
     
    }
  );
  return req;
};



module.exports.finde = finde;