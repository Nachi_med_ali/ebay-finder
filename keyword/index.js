var express = require('express');
var csv = require('csv-parser');
var fs = require('fs');
var multer = require('multer'),
	bodyParser = require('body-parser'),
	path = require('path');
var ebay = require('./scripts/Find.js').finde;

// Defining dependencies of mysql db
var inserting_req_data = require('./scripts/mysq.js').inserting_req_data;


var app = new express();
app.use(bodyParser.json());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.get('/', function(req, res){
  res.render('index');
});


app.post('/', multer({ dest: './uploads/'}).single('upl'), function(req,res){
  var data = {
    source : 'test.csv',
    process : 'ebay',
    country :  null,
    query_string : req.body.keyword,
    insert_daytime : new Date(),
    process_id : '',
    status : '9',
  }

  if (req.body.ean != null)
    data.country = req.body.ean ;
  
  inserting_req_data(data);

	var q = ebay(data);
	
  res.send('Post page');
	res.status(204).end();
});


var port = 3000;
app.listen( port, function(){ console.log('listening on port '+port); } );


